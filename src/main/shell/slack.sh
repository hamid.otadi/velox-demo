#!/bin/bash

DATA="{\"text\":\"$1 (<${CI_JOB_URL}|Job>)\"}"
curl -X POST -H 'Content-type: application/json' --data "$DATA" "$SLACK_HOOK_URL"