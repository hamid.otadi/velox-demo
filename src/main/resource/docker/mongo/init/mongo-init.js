print('Start #################################################################');

db = db.getSiblingDB('digital-upload-formats');
db.createUser(
    {
      user: "slyusr",
      pwd: "sly123456",
      roles: [
        {
          role: "readWrite",
          db: "digital-upload-formats"
        },
      ]
    }
);
db.createCollection('users');

db = db.getSiblingDB('slyorderdb');
db.createUser(
    {
      user: "slyusr",
      pwd: "sly123456",
      roles: [
        {
          role: "readWrite",
          db: "slyorderdb"
        },
      ]
    }
);
db.createCollection('users');


db = db.getSiblingDB('recommendation');
db.createUser(
    {
      user: "slyusr",
      pwd: "sly123456",
      roles: [
        {
          role: "readWrite",
          db: "recommendation"
        }
      ]
    }
);

db = db.getSiblingDB('slycatalogdb');
db.createUser(
    {
      user: "slyusr",
      pwd: "sly123456",
      roles: [
        {
          role: "readWrite",
          db: "slycatalogdb"
        }
      ]
    }
)


print('END #################################################################');