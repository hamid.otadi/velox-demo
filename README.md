# velox-demo
This project is a demo of a full shop.

## Hardware Requirements for Docker
Memory (minimum): 6 GB

## Start the service:
Execute locally with: Docker Compose

`docker-compose stop && docker-compose rm -fv && docker-compose pull && docker-compose --compatibility --env-file .env.local up --build --force-recreate --remove-orphans`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
Back-office interface is available at [http://localhost:3001/backoffice](http://localhost:3001/backoffice).